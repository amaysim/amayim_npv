// Page URL: https://www.amaysim.com.au/plans/mobile-plans/
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebForm extends PageObject{

    private final String sUsername = "0466134574";
    private final String sPassword = "AWqasde321";
    private final String sEmail = "email@sample.com";

    @FindBy(id = "username")
    private WebElement username_input;

    @FindBy(id = "password")
    private WebElement password_input;

    @FindBy(xpath = "//span[text()='Account']")
    private WebElement account_button;

    @FindBy(xpath = "//button[contains(text(),'Login')]")
    private WebElement login_button;

    @FindBy(xpath = "//h3[contains(.,'My New Sim Nickname')]")
    private WebElement restartPlan_button;

    @FindBy(xpath = "//a[contains(.,'Refer a friend')]")
    private WebElement referFriend_button;

    @FindBy(xpath = "//input[@placeholder='Send to (comma separated)']")
    private WebElement friendEmail_input;

    @FindBy(xpath = "//button[@id='c']/span/p")
    private WebElement share_button;

    @FindBy(xpath = "//div[@id='A']/span/p")
    private WebElement share_message;


    public WebForm(WebDriver driver) {
        super(driver);
    }

    public void Login(){
        this.account_button.click();
        this.username_input.sendKeys(sUsername);
        this.password_input.sendKeys(sPassword);
        this.login_button.click();
    }

    public void ReferAFriend(){
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(.,'My New Sim Nickname')]")));
        this.restartPlan_button.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(.,'Refer a friend')]")));
        this.referFriend_button.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Send to (comma separated)']")));
        this.friendEmail_input.sendKeys(sEmail);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='c']/span/p")));
        this.share_button.click();

    }

    public void verifyReferAFriend(){
       this.share_message.isDisplayed();
    }

}